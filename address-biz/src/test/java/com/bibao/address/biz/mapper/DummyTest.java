package com.bibao.address.biz.mapper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class DummyTest {
	@Test
	public void test() {
		Assert.assertEquals(100, 50 * 2);
	}
}
