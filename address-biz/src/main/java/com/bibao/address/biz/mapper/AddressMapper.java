package com.bibao.address.biz.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.persistence.entity.CityStateZipEntity;

public class AddressMapper {
	public static CityStateZip mapToCityStateZip(CityStateZipEntity entity) {
		if (entity==null) return null;
		CityStateZip cityStateZip = new CityStateZip();
		cityStateZip.setCity(entity.getCity());
		cityStateZip.setCitySequence(entity.getCitySequence());
		cityStateZip.setState(entity.getState());
		cityStateZip.setZip(entity.getZip());
		return cityStateZip;
	}
	
	public static List<CityStateZip> mapToCityStateZipList(List<CityStateZipEntity> entities) {
		List<CityStateZip> list = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(entities)) {
			entities.forEach(e -> list.add(mapToCityStateZip(e)));
		}
		return list;
	}
	
	public static String mapToStandardAddress(Address address) {
		if (address==null || !address.isValid()) return StringUtils.EMPTY;
		StringBuilder sb = new StringBuilder();
		sb.append(standardizeStreet(address.getStreet().toUpperCase()));
		if (StringUtils.isNotBlank(address.getLine2())) {
			sb.append(", " + standardizeLine2(address.getLine2().toUpperCase()));
		}
		sb.append(", " + address.getCity().toUpperCase());
		sb.append(", " + address.getState().toUpperCase());
		sb.append(" " + address.getZip());
		return sb.toString();
	}
	
	private static String standardizeStreet(String street) {
		return street.replace("ROAD", "RD")
				.replace("STREET", "ST")
				.replace("LANE", "LN");
	}
	
	private static String standardizeLine2(String line2) {
		return line2.replace("SUITE", "STE")
				.replace("APARTMENT", "APT");
	}
}
