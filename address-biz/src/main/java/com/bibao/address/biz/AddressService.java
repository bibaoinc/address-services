package com.bibao.address.biz;

import java.util.List;

import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.model.StandardAddress;

public interface AddressService {
	
	public List<CityStateZip> lookupByZip(String zip);
	
	public CityStateZip lookupByCity(String city);
	
	public StandardAddress standardizeAddress(Address address);
}
