package com.bibao.address.biz;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.address.biz.mapper.AddressMapper;
import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.model.StandardAddress;
import com.bibao.address.persistence.dao.CityStateZipDao;
import com.bibao.address.persistence.entity.CityStateZipEntity;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private CityStateZipDao zipDao;
	
	@Override
	public List<CityStateZip> lookupByZip(String zip) {
		List<CityStateZipEntity> entities = zipDao.findByZip(zip);
		return AddressMapper.mapToCityStateZipList(entities);
	}

	@Override
	public CityStateZip lookupByCity(String city) {
		CityStateZipEntity entity = zipDao.findByCity(city);
		return AddressMapper.mapToCityStateZip(entity);
	}

	@Override
	public StandardAddress standardizeAddress(Address address) {
		StandardAddress standardAddress = new StandardAddress();
		if (address.isValid()) {
			standardAddress.setAddress(AddressMapper.mapToStandardAddress(address));
			standardAddress.setInvalidReason(StringUtils.EMPTY);
		} else {
			standardAddress.setAddress(StringUtils.EMPTY);
			StringBuilder reason = new StringBuilder("Missing ");
			if (StringUtils.isBlank(address.getStreet())) {
				reason.append("street,");
			}
			if (StringUtils.isBlank(address.getCity())) {
				reason.append("city,");
			}
			if (StringUtils.isBlank(address.getState())) {
				reason.append("state,");
			}
			if (StringUtils.isBlank(address.getZip())) {
				reason.append("zip,");
			}
			standardAddress.setInvalidReason(reason.toString().substring(0, reason.length() - 1));
		}
		return standardAddress;
	}

}
