drop table citystatezip;

drop sequence citystatezip_seq;

create table citystatezip (
	id number(4) primary key,
	zip varchar2(10) not null,
	city varchar2(20) not null,
	state char(2) not null,
	city_sequence number(2) not null
);

create sequence citystatezip_seq
minvalue 1
start with 1;

insert into citystatezip values (citystatezip_seq.nextval, '08536', 'PLAINSBORO', 'NJ', 1);
insert into citystatezip values (citystatezip_seq.nextval, '08540', 'PRINCETON TWP', 'NJ', 1);
insert into citystatezip values (citystatezip_seq.nextval, '08540', 'PRINCETON TOWNSHIP', 'NJ', 2);
insert into citystatezip values (citystatezip_seq.nextval, '08540', 'PRINCETON', 'NJ', 3);
insert into citystatezip values (citystatezip_seq.nextval, '08550', 'WEST WINDSOR', 'NJ', 1);
insert into citystatezip values (citystatezip_seq.nextval, '08550', 'PRINCETON JCT', 'NJ', 2);
insert into citystatezip values (citystatezip_seq.nextval, '08550', 'PRINCETON JUNCTION', 'NJ', 3);
insert into citystatezip values (citystatezip_seq.nextval, '08550', 'W WINDSOR TOWNSHIP', 'NJ', 4);
insert into citystatezip values (citystatezip_seq.nextval, '08550', 'W WINDSOR', 'NJ', 5);
insert into citystatezip values (citystatezip_seq.nextval, '27556', 'MIDDLEBURG', 'NC', 1);
insert into citystatezip values (citystatezip_seq.nextval, '27519', 'CARY', 'NC', 1);

