package com.bibao.address.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.bibao.address.contract.AddressRequest;
import com.bibao.address.contract.AddressResponse;
import com.bibao.address.contract.CityStateZipResponse;
import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.model.StandardAddress;

public class AddressRestMapper {
	public static CityStateZipResponse mapToResponse(CityStateZip cityStateZip) {
		if (cityStateZip==null) return null;
		CityStateZipResponse response = new CityStateZipResponse();
		response.setCity(cityStateZip.getCity());
		response.setCitySequence(cityStateZip.getCitySequence());
		response.setState(cityStateZip.getState());
		response.setZip(cityStateZip.getZip());
		return response;
	}
	
	public static List<CityStateZipResponse> mapToResponses(List<CityStateZip> cityStateZipList) {
		List<CityStateZipResponse> responses = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(cityStateZipList)) {
			cityStateZipList.forEach(cityStateZip -> responses.add(mapToResponse(cityStateZip)));
		}
		return responses;
	}
	
	public static Address mapToAddress(AddressRequest request) {
		Address address = new Address();
		address.setStreet(request.getStreet());
		address.setLine2(request.getLine2());
		address.setCity(request.getCity());
		address.setState(request.getState());
		address.setZip(request.getZip());
		return address;
	}
	
	public static AddressResponse mapToAddressResponse(StandardAddress address) {
		AddressResponse response = new AddressResponse();
		response.setAddress(address.getAddress());
		response.setInvalidReason(address.getInvalidReason());
		return response;
	}
}
