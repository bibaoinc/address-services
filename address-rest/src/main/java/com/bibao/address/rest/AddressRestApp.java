package com.bibao.address.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.address.rest.config.AddressRestConfig;

@SpringBootApplication
@Import(AddressRestConfig.class)
public class AddressRestApp {
	public static void main(String[] args) {
		SpringApplication.run(AddressRestApp.class, args);
	}
}
