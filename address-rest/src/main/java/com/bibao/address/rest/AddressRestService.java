package com.bibao.address.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.bibao.address.biz.AddressService;
import com.bibao.address.contract.AddressRequest;
import com.bibao.address.contract.AddressResponse;
import com.bibao.address.contract.CityStateZipResponse;
import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.rest.mapper.AddressRestMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/address")
@Api(value="Address Services")
public class AddressRestService {
	@Autowired
	private AddressService service;
	
	@GET
	@Path("/lookup/zip/{zip}")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@ApiOperation(value = "Get City and State using zip", response = CityStateZipResponse.class, notes = "Use zip code")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "City found for the zip", response = CityStateZipResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Bad Request", response = Response.Status.class),
			@ApiResponse(code = 500, message = "Server exception", response = Response.Status.class)})
	public Response getCityStateforZipCode(
			@ApiParam(value = "The zip code", required = true) @PathParam("zip") String zip) {
		if (StringUtils.isBlank(zip)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		List<CityStateZip> list = service.lookupByZip(zip);
		List<CityStateZipResponse> responses = AddressRestMapper.mapToResponses(list);
		GenericEntity<List<CityStateZipResponse>> entity = new GenericEntity<List<CityStateZipResponse>>(responses) {};

		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/lookup/city")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@ApiOperation(value = "Get State and Zip using city", response = CityStateZipResponse.class, notes = "Use city name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "State and Zip found for the city", response = CityStateZipResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Response.Status.class),
			@ApiResponse(code = 500, message = "Server exception", response = Response.Status.class)})
	public Response getCityStateforCity(
			@ApiParam(value = "The city name", required = true) @QueryParam("city") String city) {
		if (StringUtils.isBlank(city)) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		CityStateZip cityStateZip = service.lookupByCity(city.toUpperCase());
		CityStateZipResponse response = AddressRestMapper.mapToResponse(cityStateZip);
		
		return Response.ok(response).build();
	}
	
	@POST
	@Path("/standardize")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@ApiOperation(value = "Standardize Address", response = AddressResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Address standardized", response = AddressResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Response.Status.class),
			@ApiResponse(code = 500, message = "Server exception", response = Response.Status.class)})
	public Response standardize(AddressRequest request) {
		Address address = AddressRestMapper.mapToAddress(request);
		AddressResponse response = AddressRestMapper.mapToAddressResponse(service.standardizeAddress(address));
		
		return Response.ok(response).build();
	}
	
	@GET
	@Produces("text/plain")
	@Path("/ping")
	public String ping() {
		return "Sucess";
	}
}
