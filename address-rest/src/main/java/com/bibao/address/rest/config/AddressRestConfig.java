package com.bibao.address.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.bibao.address.biz.config.AddressBizConfig;
import com.bibao.address.persistence.config.JPAConfig;

@Configuration
@ComponentScan(basePackages = "com.bibao.address.rest")
@Import(value = { JerseyConfig.class, AddressBizConfig.class, JPAConfig.class})
public class AddressRestConfig {

}
