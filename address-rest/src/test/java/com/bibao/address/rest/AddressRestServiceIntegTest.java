package com.bibao.address.rest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.address.contract.AddressRequest;
import com.bibao.address.contract.AddressResponse;
import com.bibao.address.contract.CityStateZipResponse;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = AddressRestApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddressRestServiceIntegTest {
	@Autowired
	private TestRestTemplate restTemplate;
	
	private final String ADDRESS_URL = "/address-rest/address/";
	
	@SuppressWarnings("unchecked")
	@Description("Test method getCityStateforZipCode")
	@Test
	public void testGetCityStateforZipCode() {
		String url = ADDRESS_URL + "/lookup/zip/08536";
		ResponseEntity<Object[]> exchange = restTemplate.getForEntity(url, Object[].class);
		assertEquals(exchange.getStatusCodeValue(), HttpStatus.OK.value());
		Object[] responses = (Object[])exchange.getBody();
		assertEquals(1, responses.length);
		Map<String, Object> map = (Map<String, Object>)responses[0];
		assertEquals(1, ((Integer)map.get("citySequence")).intValue());
		assertEquals("PLAINSBORO", map.get("city"));
		assertEquals("NJ", map.get("state"));
		assertEquals("08536", map.get("zip"));
	}
	
	@Description("Test method getCityStateforCity")
	@Test
	public void testGetCityStateforCity() {
		HttpHeaders httpHeaders = new HttpHeaders();
		String url = ADDRESS_URL + "/lookup/city?city=Cary";
		ResponseEntity<CityStateZipResponse> exchange = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(httpHeaders), CityStateZipResponse.class);
		assertEquals(exchange.getStatusCodeValue(), HttpStatus.OK.value());
		CityStateZipResponse response = exchange.getBody();
		assertEquals(1, response.getCitySequence());
		assertEquals("CARY", response.getCity());
		assertEquals("NC", response.getState());
		assertEquals("27519", response.getZip());
	}
	
	@Description("Test method standardize")
	@Test
	public void testStandardize() {
		String url = ADDRESS_URL + "/standardize";
		AddressRequest request = new AddressRequest();
		request.setStreet("100 Test Lane");
		request.setCity("Silver Spring");
		request.setState("MD");
		request.setZip("56789");
		ResponseEntity<AddressResponse> exchange = restTemplate.postForEntity(url, request, AddressResponse.class);
		assertEquals(exchange.getStatusCodeValue(), HttpStatus.OK.value());
		AddressResponse response = exchange.getBody();
		assertEquals("100 TEST LN, SILVER SPRING, MD 56789", response.getAddress());
		assertEquals(StringUtils.EMPTY, response.getInvalidReason());
	}
}
