package com.bibao.address.rest.provider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.address.persistence.dao.CityStateZipDao;
import com.bibao.address.persistence.entity.CityStateZipEntity;
import com.bibao.address.rest.AddressRestApp;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "pact.provider.version=1.0.1",
                "pact.verifier.publishResults=true",
                "spring.profiles.active=test"
        })
@ContextConfiguration(classes = AddressRestApp.class)
@Provider("address_provider")
@PactFolder("target/pacts")
public class AddressProviderPactTest {
	private static final Logger LOG = LoggerFactory.getLogger(AddressProviderPactTest.class);
	
	@LocalServerPort
    private int portNumber;
	
	@MockBean
	private CityStateZipDao dao;

    @BeforeEach
    void setupTestTarget(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", portNumber, ""));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State({"City State For Zip Code"})
    public void getCityStateZipState() {
    	Mockito.when(dao.findByZip(Mockito.anyString())).thenReturn(mockResultForZip());
    }
    
    @State({"City State For City"})
    public void getCityStateCityState() {
    	Mockito.when(dao.findByCity(Mockito.anyString())).thenReturn(readEntityFromFile());
    }
    
    @State({"Standardize Address"})
    public void getStandardizeState() {
    	
    }
    
    private List<CityStateZipEntity> mockResultForZip() {
    	List<CityStateZipEntity> entities = new ArrayList<>();
    	entities.add(readEntityFromFile());
    	entities.add(createEntity("12345", "Home City TWP", "FL", 2));
    	return entities;
    }
    
    private CityStateZipEntity createEntity(String zip, String city, String state, int citySequence) {
		CityStateZipEntity entity = new CityStateZipEntity();
		entity.setZip(zip);
		entity.setCity(city);
		entity.setState(state);
		entity.setCitySequence(citySequence);
		return entity;
	}
    
    private CityStateZipEntity readEntityFromFile() {
    	ObjectMapper mapper = new ObjectMapper();
    	CityStateZipEntity entity = null;
    	try {
    		File base = new File("");
    		File inputFile = new File(base.getAbsolutePath() + "/src/test/resources/zip2citystate.json");
    		entity = mapper.readValue(inputFile, CityStateZipEntity.class);
    	} catch (IOException e) {
    		LOG.error("Error: {}", e.getMessage());
    	}
    	return entity;
    }
}
