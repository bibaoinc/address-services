package com.bibao.address.rest.provider;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.address.rest.AddressRestApp;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "pact.provider.version=1.0.1",
                "pact.verifier.publishResults=true",
        })
@ContextConfiguration(classes = AddressRestApp.class)
@Provider("address_service_provider")
@PactFolder("target/pacts")
public class AddressProviderPactIntegTest {
	@LocalServerPort
    private int portNumber;
	
    @BeforeEach
    void setupTestTarget(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", portNumber, ""));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State({"City State For Zip Code"})
    public void getCityStateZipState() {
    	
    }
    
    @State({"City State For City"})
    public void getCityStateCityState() {
    	
    }
    
    @State({"Standardize Address"})
    public void getStandardizeState() {
    	
    }
    
}
