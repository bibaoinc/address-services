package com.bibao.address.rest.consumer;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.DslPart;
import au.com.dius.pact.consumer.dsl.PactDslResponse;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;
import io.pactfoundation.consumer.dsl.LambdaDsl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.CoreMatchers.is;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "address_provider")
public class AddressConsumerPactTest {
	private final String CITY_STATE_ZIP_URL = "/address-rest/address/lookup/";
	private final String ADDRESS_URL = "/address-rest/address/standardize";
	
	private String[] zipRequests = { "08536", "27519", "08540", "08550"};
	private String[] cityRequests = { "city=Plainsboro", "city=Cary", "city=Princeton", "city=Princeton JCT"};
	
	private String[] addressRequests = {
			"{\"street\":\"42 Tennyson Road\",\"city\":\"Plainsboro\",\"state\":\"NJ\",\"zip\":\"08536\"}",
			"{\"street\":\"56 Main Street\",\"line2\":\"Suite 102\",\"city\":\"Flushing\",\"state\":\"NY\",\"zip\":\"13482\"}",
			"{\"city\":\"Greensboro\",\"state\":\"NY\",\"zip\":\"13482\"}",
			"{\"street\":\"1 Main Road\",\"state\":\"NY\"}"
	};
	
	@Pact(consumer = "zip_consumer")
    public RequestResponsePact cityStateForZipCode(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        DslPart dslPartResponse = LambdaDsl.newJsonArrayMinLike(1, value -> 
        			value.object(o -> o.stringType("city", "state", "zip").numberType("citySequence"))
        		).build();
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<zipRequests.length; i++) {
        	String url = CITY_STATE_ZIP_URL + "zip/" + zipRequests[i];
        	if (i==0) {
        		pactDslResponse = builder.given("City State For Zip Code")
                        			.uponReceiving("A request to get city state for zip code")
                        			.method("GET")
                        			.headers(requestHeaders)
                        			.path(url)
                        			.willRespondWith()
                        			.status(200)
                        			.body(dslPartResponse);
        	} else {
        		pactDslResponse = pactDslResponse.given("City State For Zip Code")
            			.uponReceiving("A request to get city state for zip code")
            			.method("GET")
            			.headers(requestHeaders)
            			.path(url)
            			.willRespondWith()
            			.status(200)
            			.body(dslPartResponse);
        	}
        }
        return pactDslResponse.toPact();
    }
	
	@Test
    @PactTestFor(pactMethod = "cityStateForZipCode")
    public void testGetCityStateForZipCode(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
		
		for (int i=0; i<zipRequests.length; i++) {
			String url = CITY_STATE_ZIP_URL + "zip/" + zipRequests[i];
			HttpResponse httpResponse = Request.Get(mockServer.getUrl() + url)
	                .setHeaders(headers)
	                .execute().returnResponse();
	        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
        }
		
    }
	
	@Pact(consumer = "city_consumer")
    public RequestResponsePact cityStateForCity(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        DslPart dslPartResponse = LambdaDsl.newJsonBody(value -> 
        			value.stringType("city", "state", "zip").numberType("citySequence")
        		).build();
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<cityRequests.length; i++) {
        	String url = CITY_STATE_ZIP_URL + "city";
        	if (i==0) {
        		pactDslResponse = builder.given("City State For City")
                        			.uponReceiving("A request to get city state for zip code")
                        			.method("GET")
                        			.headers(requestHeaders)
                        			.path(url)
                        			.query(cityRequests[i])
                        			.willRespondWith()
                        			.status(200)
                        			.body(dslPartResponse);
        	} else {
        		pactDslResponse = pactDslResponse.given("City State For City")
            			.uponReceiving("A request to get city state for zip code")
            			.method("GET")
            			.headers(requestHeaders)
            			.path(url)
            			.query(cityRequests[i])
            			.willRespondWith()
            			.status(200)
            			.body(dslPartResponse);
        	}
        }
        return pactDslResponse.toPact();
    }
	
//	@Test
//    @PactTestFor(pactMethod = "cityStateForCity")
//    public void testGetCityStateForCity(MockServer mockServer) throws Exception {
//		Header[] headers = {
//				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
//		};
//		
//		for (int i=0; i<cityRequests.length; i++) {
//			String url = CITY_STATE_ZIP_URL + "city?" + cityRequests[i];
//			HttpResponse httpResponse = Request.Get(mockServer.getUrl() + url)
//	                .setHeaders(headers)
//	                .execute().returnResponse();
//	        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
//        }
//		
//    }
	
	@Test
    @PactTestFor(pactMethod = "cityStateForCity")
    public void testGetCityStateForCity(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
        
		for (int i=0; i<cityRequests.length; i++) {
	        URIBuilder builder = new URIBuilder();
	        List<NameValuePair> params = new ArrayList<>();
	        params.add(new BasicNameValuePair("city", buildParams(cityRequests[i])));
	        
	        builder.setScheme("http").setHost("127.0.0.1")
	                .setPort(mockServer.getPort())
	                .setPath(CITY_STATE_ZIP_URL + "city")
	                .setParameters(params);
	        		
	        URI uri = builder.build();
	        HttpGet httpget = new HttpGet(uri);
	        httpget.setHeaders(headers);
	        CloseableHttpClient httpclient = HttpClients.createDefault();
	        CloseableHttpResponse response = httpclient.execute(httpget);
	        assertThat(response.getStatusLine().getStatusCode(), is(equalTo(200)));
		}
    }
	
	private String buildParams(String queryParam) {
		String[] tokens = queryParam.split("=");
		return tokens[1];
	}
	
	@Pact(consumer = "address_consumer")
    public RequestResponsePact standardize(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        DslPart dslPartResponse = LambdaDsl.newJsonBody(value -> 
					value.stringType("address")
						.stringType("invalidReason")
        		).build();
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<addressRequests.length; i++) {
        	if (i==0) {
        		pactDslResponse = builder.given("Standardize Address")
                        			.uponReceiving("A request to standardize address")
                        			.method("POST")
                        			.headers(requestHeaders)
                        			.body(addressRequests[i])
                        			.path(ADDRESS_URL)
                        			.willRespondWith()
                        			.status(200)
                        			.body(dslPartResponse);
        	} else {
        		pactDslResponse = pactDslResponse.given("Standardize Address")
            			.uponReceiving("A request to standardize address")
            			.method("POST")
            			.headers(requestHeaders)
            			.body(addressRequests[i])
            			.path(ADDRESS_URL)
            			.willRespondWith()
            			.status(200)
            			.body(dslPartResponse);
        	}
        }
        return pactDslResponse.toPact();
    }
	
	@Test
    @PactTestFor(pactMethod = "standardize")
    public void testStandardize(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
		
		for (String payload: addressRequests) {
			HttpResponse httpResponse = Request.Post(mockServer.getUrl() + ADDRESS_URL)
	                .setHeaders(headers)
	                .bodyString(payload, ContentType.APPLICATION_JSON)
	                .execute().returnResponse();
	        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
        }
		
    }
}
