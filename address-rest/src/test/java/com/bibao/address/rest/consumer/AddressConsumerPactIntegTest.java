package com.bibao.address.rest.consumer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.dsl.PactDslResponse;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.model.RequestResponsePact;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "address_service_provider")
public class AddressConsumerPactIntegTest {
	private final String CITY_STATE_ZIP_URL = "/address-rest/address/lookup/";
	private final String ADDRESS_URL = "/address-rest/address/standardize";
	
	private String[] zipRequests = { "08536", "27519", "08540", "08550"};
	private String[] zipResponses = {
			"[{\"city\":\"PLAINSBORO\",\"state\":\"NJ\",\"zip\":\"08536\",\"citySequence\":1}]", 
			"[{\"city\":\"CARY\",\"state\":\"NC\",\"zip\":\"27519\",\"citySequence\":1}]",
			"[{\"city\":\"PRINCETON TWP\",\"state\":\"NJ\",\"zip\":\"08540\",\"citySequence\":1},{\"city\":\"PRINCETON TOWNSHIP\",\"state\":\"NJ\",\"zip\":\"08540\",\"citySequence\":2},{\"city\":\"PRINCETON\",\"state\":\"NJ\",\"zip\":\"08540\",\"citySequence\":3}]",
			"[{\"city\":\"WEST WINDSOR\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":1},{\"city\":\"PRINCETON JCT\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":2},{\"city\":\"PRINCETON JUNCTION\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":3},{\"city\":\"W WINDSOR TOWNSHIP\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":4},{\"city\":\"W WINDSOR\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":5}]"
	};
	
	private String[] cityRequests = { "city=Plainsboro", "city=Cary", "city=Princeton", "city=Princeton JCT"};
	private String[] cityResponses = {
			"{\"city\":\"PLAINSBORO\",\"state\":\"NJ\",\"zip\":\"08536\",\"citySequence\":1}",
			"{\"city\":\"CARY\",\"state\":\"NC\",\"zip\":\"27519\",\"citySequence\":1}",
			"{\"city\":\"PRINCETON\",\"state\":\"NJ\",\"zip\":\"08540\",\"citySequence\":3}",
			"{\"city\":\"PRINCETON JCT\",\"state\":\"NJ\",\"zip\":\"08550\",\"citySequence\":2}"
	};
	
	private String[] addressRequests = {
			"{\"street\":\"42 Tennyson Road\",\"city\":\"Plainsboro\",\"state\":\"NJ\",\"zip\":\"08536\"}",
			"{\"street\":\"56 Main Street\",\"line2\":\"Suite 102\",\"city\":\"Flushing\",\"state\":\"NY\",\"zip\":\"13482\"}",
			"{\"city\":\"Greensboro\",\"state\":\"NY\",\"zip\":\"13482\"}",
			"{\"street\":\"1 Main Road\",\"state\":\"NY\"}"
	};
	private String[] addressResponses = {
			"{\"address\":\"42 TENNYSON RD, PLAINSBORO, NJ 08536\",\"invalidReason\":\"\"}",
			"{\"address\":\"56 MAIN ST, STE 102, FLUSHING, NY 13482\",\"invalidReason\":\"\"}",
			"{\"address\":\"\",\"invalidReason\":\"Missing street\"}",
			"{\"address\":\"\",\"invalidReason\":\"Missing city,zip\"}",
	};
	
	@Pact(consumer = "zip_consumer")
    public RequestResponsePact cityStateForZipCode(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<zipRequests.length; i++) {
        	String url = CITY_STATE_ZIP_URL + "zip/" + zipRequests[i];
        	if (i==0) {
        		pactDslResponse = builder.given("City State For Zip Code")
                        			.uponReceiving("A request to get city state for zip code")
                        			.method("GET")
                        			.headers(requestHeaders)
                        			.path(url)
                        			.willRespondWith()
                        			.status(200)
                        			.body(zipResponses[i]);
        	} else {
        		pactDslResponse = pactDslResponse.given("City State For Zip Code")
            			.uponReceiving("A request to get city state for zip code")
            			.method("GET")
            			.headers(requestHeaders)
            			.path(url)
            			.willRespondWith()
            			.status(200)
            			.body(zipResponses[i]);
        	}
        }
        return pactDslResponse.toPact();
        
//        return builder.given("City State For Zip Code")
//    			.uponReceiving("A request to get city state for zip code")
//    			.method("GET")
//    			.headers(requestHeaders)
//    			.path(CITY_STATE_ZIP_URL + "zip/" + zipRequests[0])
//    			.willRespondWith()
//    			.status(200)
//    			.body(zipResponses[0])
//    			.toPact();
    }
	
	@Test
    @PactTestFor(pactMethod = "cityStateForZipCode")
    public void testGetCityStateForZipCode(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
		
		for (int i=0; i<zipRequests.length; i++) {
			String url = CITY_STATE_ZIP_URL + "zip/" + zipRequests[i];
			HttpResponse httpResponse = Request.Get(mockServer.getUrl() + url)
	                .setHeaders(headers)
	                .execute().returnResponse();
	        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
        }
		
//		HttpResponse httpResponse = Request.Get(mockServer.getUrl() + CITY_STATE_ZIP_URL + "zip/" + zipRequests[0])
//                .setHeaders(headers)
//                .execute().returnResponse();
//        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
    }
	
	@Pact(consumer = "city_consumer")
    public RequestResponsePact cityStateForCity(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<cityRequests.length; i++) {
        	String url = CITY_STATE_ZIP_URL + "city";
        	if (i==0) {
        		pactDslResponse = builder.given("City State For City")
                        			.uponReceiving("A request to get city state for zip code")
                        			.method("GET")
                        			.headers(requestHeaders)
                        			.path(url)
                        			.query(cityRequests[i])
                        			.willRespondWith()
                        			.status(200)
                        			.body(cityResponses[i]);
        	} else {
        		pactDslResponse = pactDslResponse.given("City State For City")
            			.uponReceiving("A request to get city state for zip code")
            			.method("GET")
            			.headers(requestHeaders)
            			.path(url)
            			.query(cityRequests[i])
            			.willRespondWith()
            			.status(200)
            			.body(cityResponses[i]);
        	}
        }
        return pactDslResponse.toPact();
    }
	
	@Test
    @PactTestFor(pactMethod = "cityStateForCity")
    public void testGetCityStateForCity(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
        
		for (int i=0; i<cityRequests.length; i++) {
	        URIBuilder builder = new URIBuilder();
	        List<NameValuePair> params = new ArrayList<>();
	        params.add(new BasicNameValuePair("city", buildParams(cityRequests[i])));
	        
	        builder.setScheme("http").setHost("127.0.0.1")
	                .setPort(mockServer.getPort())
	                .setPath(CITY_STATE_ZIP_URL + "city")
	                .setParameters(params);
	        		
	        URI uri = builder.build();
	        HttpGet httpget = new HttpGet(uri);
	        httpget.setHeaders(headers);
	        CloseableHttpClient httpclient = HttpClients.createDefault();
	        CloseableHttpResponse response = httpclient.execute(httpget);
	        assertThat(response.getStatusLine().getStatusCode(), is(equalTo(200)));
		}
    }
	
	private String buildParams(String queryParam) {
		String[] tokens = queryParam.split("=");
		return tokens[1];
	}
	
	@Pact(consumer = "address_consumer")
    public RequestResponsePact standardize(PactDslWithProvider builder) {			

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json; charset=UTF-8");
        
        PactDslResponse pactDslResponse = null;
        for (int i=0; i<addressRequests.length; i++) {
        	if (i==0) {
        		pactDslResponse = builder.given("Standardize Address")
                        			.uponReceiving("A request to standardize address")
                        			.method("POST")
                        			.headers(requestHeaders)
                        			.body(addressRequests[i])
                        			.path(ADDRESS_URL)
                        			.willRespondWith()
                        			.status(200)
                        			.body(addressResponses[i]);
        	} else {
        		pactDslResponse = pactDslResponse.given("Standardize Address")
            			.uponReceiving("A request to standardize address")
            			.method("POST")
            			.headers(requestHeaders)
            			.body(addressRequests[i])
            			.path(ADDRESS_URL)
            			.willRespondWith()
            			.status(200)
            			.body(addressResponses[i]);
        	}
        }
        return pactDslResponse.toPact();
    }
	
	@Test
    @PactTestFor(pactMethod = "standardize")
    public void testStandardize(MockServer mockServer) throws Exception {
		Header[] headers = {
				new BasicHeader("Content-type", "application/json; charset=UTF-8"),
		};
		
		for (String payload: addressRequests) {
			HttpResponse httpResponse = Request.Post(mockServer.getUrl() + ADDRESS_URL)
	                .setHeaders(headers)
	                .bodyString(payload, ContentType.APPLICATION_JSON)
	                .execute().returnResponse();
	        assertThat(httpResponse.getStatusLine().getStatusCode(), is(equalTo(200)));
        }
		
    }
}
