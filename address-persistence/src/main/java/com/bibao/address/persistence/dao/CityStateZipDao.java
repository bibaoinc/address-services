package com.bibao.address.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bibao.address.persistence.entity.CityStateZipEntity;

public interface CityStateZipDao extends JpaRepository<CityStateZipEntity, Integer> {
	
	public List<CityStateZipEntity> findByZip(String zip);
	
	public CityStateZipEntity findByCity(String city);
}
