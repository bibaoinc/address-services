package com.bibao.address.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.address.persistence.config.JPAConfig;
import com.bibao.address.persistence.entity.CityStateZipEntity;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = JPAConfig.class)
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
public class CityStateZipDaoTest {
	@Autowired
	private CityStateZipDao dao;
	
	@BeforeEach
	public void setUp() throws Exception {
		List<CityStateZipEntity> entities = new ArrayList<>();
		entities.add(createEntity("12345", "Newark", "NJ"));
		entities.add(createEntity("12345", "Newark TWP", "NJ"));
		entities.add(createEntity("56789", "New York", "NY"));
		entities.add(createEntity("14578", "Durham", "NC"));
		dao.saveAll(entities);
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		dao.deleteAll();
	}
	
	@Description("Test method findByZip")
	@Test
	public void testFindByZip() {
		List<CityStateZipEntity> entities = dao.findByZip("12345");
		assertEquals(2, entities.size());
		entities = dao.findByZip("56789");
		assertEquals(1, entities.size());	
		CityStateZipEntity entity = entities.get(0);
		assertEquals("New York", entity.getCity());
		assertEquals("NY", entity.getState());
		assertEquals(1, entity.getCitySequence());
	}
	
	@Description("Test method findByCity")
	@Test
	public void testFindByCity() {
		CityStateZipEntity entity = dao.findByCity("Durham");
		assertNotNull(entity);
		assertEquals("14578", entity.getZip());
		assertEquals("NC", entity.getState());
		assertEquals(1, entity.getCitySequence());
		entity = dao.findByCity("ABC");
		assertNull(entity);
	}
	
	private CityStateZipEntity createEntity(String zip, String city, String state) {
		CityStateZipEntity entity = new CityStateZipEntity();
		entity.setZip(zip);
		entity.setCity(city);
		entity.setState(state);
		entity.setCitySequence(1);
		return entity;
	}
}
