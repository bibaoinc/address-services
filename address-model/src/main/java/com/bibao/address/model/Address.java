package com.bibao.address.model;

import org.apache.commons.lang3.StringUtils;

public class Address {
	private String street;
	private String line2;
	private String city;
	private String state;
	private String zip;
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public boolean isValid() {
		return StringUtils.isNotBlank(street) && StringUtils.isNotBlank(city) 
				&& StringUtils.isNotBlank(state) && StringUtils.isNotBlank(zip);
	}
}
