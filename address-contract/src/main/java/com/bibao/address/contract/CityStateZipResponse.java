package com.bibao.address.contract;

public class CityStateZipResponse {
	private String city;
	private String state;
	private String zip;
	private int citySequence;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public int getCitySequence() {
		return citySequence;
	}
	public void setCitySequence(int citySequence) {
		this.citySequence = citySequence;
	}
	
}
